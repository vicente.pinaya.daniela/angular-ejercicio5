import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  nombre:any = {
    nombrecompleto:'Pepito Grillo Consciencia',
    direccion:'AV. Pinocho'
  };

  mostrar:boolean=true;

  constructor() { }

  ngOnInit(): void {
  }

}
